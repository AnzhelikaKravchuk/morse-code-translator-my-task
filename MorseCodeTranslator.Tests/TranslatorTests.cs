﻿using System;
using System.Collections;
using NUnit.Framework;

// ReSharper disable StringLiteralTypo
#pragma warning disable CA1707

namespace MorseCodeTranslator.Tests
{
    [TestFixture]
    public class TranslatorTests
    {
        [Test]
        public void TranslateTextToStandardMorse_TextIsNull_ThrowsArgumentNullException()
        {
            // Act
            Assert.Throws<ArgumentNullException>(() => Translator.TranslateTextToMorse(null));
        }

        [Test]
        public void TranslateStandardMorseToText_MorseIsNull_ThrowsArgumentNullException()
        {
            // Act
            Assert.Throws<ArgumentNullException>(() => Translator.TranslateMorseToText(null));
        }

        [TestCase(" ")]
        public void TranslateStandardMorseToText_MorseConsistOfOnlySymbolsSeparator_ThrowsArgumentException(string morseCode)
        {
            // Act
            Assert.Throws<ArgumentException>(() => Translator.TranslateMorseToText(morseCode));
        }

        [TestCase(".... . -.------ .-.. ---")]
        public void TranslateStandardMorseToText_ContainsNonExistingMorseCodes_ThrowsArgumentException(string morseCode)
        {
            // Act
            Assert.Throws<ArgumentException>(() => Translator.TranslateMorseToText(morseCode));
        }

        [TestCase("*.... . .-.. .-.. ---")]
        [TestCase("....*. .-.. .-.. ---")]
        [TestCase(".... . *.-.. .-.. ---")]
        [TestCase(".... . * .-.. .-.. ---")]
        [TestCase(".... . *=*.-.. .-.. ---==")]
        [TestCase(".... . *=* .-.. .-.. ---==")]
        public void TranslateStandardMorseToText_ContainsInvalidSymbols_ThrowsArgumentException(string morseCode)
        {
            // Act
            Assert.Throws<ArgumentException>(() => Translator.TranslateMorseToText(morseCode));
        }

        [TestCase("1**** * *=** *=** ===")]
        [TestCase("**** * -*=** *=** ===")]
        [TestCase("**** * - *=** *=** ===")]
        [TestCase("**** * -.*=** *=** ===")]
        [TestCase("**** * -.- *=** *=** ===")]
        public void TranslateCustomMorseToText_ContainsInvalidSymbols_ThrowsArgumentException(string morseCode)
        {
            // Act
            Assert.Throws<ArgumentException>(() => Translator.TranslateMorseToText(morseCode, new MorseCodeStyle('*', '=', ' ', '|')));
        }

        [TestCase(" .... . .-.. .-.. ---")]
        [TestCase(".... . .-.. .-.. --- ")]
        [TestCase(" .... . .-.. .-.. --- ")]
        public void TranslateStandardMorseToText_SymbolsSeparatorsInBeginningOrEnd_ThrowsArgumentException(string morseCode)
        {
            // Act
            Assert.Throws<ArgumentException>(() => Translator.TranslateMorseToText(morseCode));
        }

        [TestCase("....  . .-.. .-.. ---")]
        [TestCase("....    . .-.. .-.. ---")]
        [TestCase("   .... . .-.. .-.. ---")]
        [TestCase(".... . .-.. .-.. ---   ")]
        public void TranslateStandardMorseToText_ContainsMultipleSymbolsSeparatorsNextToEachOther_ThrowsArgumentException(string morseCode)
        {
            // Act
            Assert.Throws<ArgumentException>(() => Translator.TranslateMorseToText(morseCode));
        }

        [TestCase(".... . .-.. .-.. ---||.-- --- .-. .-.. -..")]
        [TestCase(".... . .-.. .-.. ---||||.-- --- .-. .-.. -..")]
        public void TranslateStandardMorseToText_ContainsMultipleWordsSeparatorsNextToEachOther_ThrowsArgumentException(string morseCode)
        {
            // Act
            Assert.Throws<ArgumentException>(() => Translator.TranslateMorseToText(morseCode));
        }

        [TestCase(".... . .-.. .-.. ---| .-- --- .-. .-.. -..")]
        [TestCase(".... . .-.. .-.. --- |.-- --- .-. .-.. -..")]
        [TestCase(".... . .-.. .-.. --- | .-- --- .-. .-.. -..")]
        public void TranslateStandardMorseToText_MorseContainsSymbolSeparatorNextToWordSeparator_ThrowsArgumentException(string morseCode)
        {
            // Act
            Assert.Throws<ArgumentException>(() => Translator.TranslateMorseToText(morseCode));
        }

        [TestCaseSource(typeof(TestCasesSourceMorseBitArrayTranslators), nameof(TestCasesSourceMorseBitArrayTranslators.TestCasesBitArrayToCustomMorseBoolArrayConsistsOfOnlyOneFalse))]
        public void TranslateBoolArrayToMorse_BoolArrayConsistsOfOnlyOneFalse_ThrowsArgumentException(BitArray morseCode)
        {
            // Act
            Assert.Throws<ArgumentException>(() => Translator.TranslateBitArrayToMorse(new BitArray(morseCode)));
        }

        [TestCaseSource(typeof(TestCasesSourceMorseBitArrayTranslators), nameof(TestCasesSourceMorseBitArrayTranslators.TestCasesBitArrayToCustomMorseBoolArrayHasOnlyOneFalseOnTheEdge))]
        public void TranslateBoolArrayToMorse_BoolArrayHasOnlyOneFalseOnTheEdge_ThrowsArgumentException(BitArray morseCode)
        {
            // Act
            Assert.Throws<ArgumentException>(() => Translator.TranslateBitArrayToMorse(new BitArray(morseCode)));
        }

        [TestCaseSource(typeof(TestCasesSourceMorseBitArrayTranslators), nameof(TestCasesSourceMorseBitArrayTranslators.TestCasesBitArrayToCustomMorseBoolArrayContainsNonExistingSymbol))]
        public void TranslateBoolArrayToMorse_BoolArrayContainsNonExistingSymbol_ThrowsArgumentException(BitArray morseCode)
        {
            // Act
            Assert.Throws<ArgumentException>(() => Translator.TranslateBitArrayToMorse(new BitArray(morseCode)));
        }

        [TestCaseSource(typeof(TestCasesSourseTextMorseTranslators), nameof(TestCasesSourseTextMorseTranslators.TestCasesTextToStandartMorse))]
        public void TranslateTextToStandartMorse_ParametersAreValid_ReturnsMorse(string message, string expectedResult)
        {
            // Act
            string actualResult = Translator.TranslateTextToMorse(message);

            // Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestCaseSource(typeof(TestCasesSourseTextMorseTranslators), nameof(TestCasesSourseTextMorseTranslators.TestCasesStandartMorseToText))]
        public void TranslateStandartMorseToText_ParametersAreValid_ReturnsText(string message, string expectedResult)
        {
            // Act
            string actualResult = Translator.TranslateMorseToText(message);

            // Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestCaseSource(typeof(TestCasesSourseTextMorseTranslators), nameof(TestCasesSourseTextMorseTranslators.TestCasesTextToCustomMorse))]
        public void TranslateTextToCustomMorse_ParametersAreValid_ReturnsCustomMorse(string message, string expectedResult)
        {
            // Act
            string actualResult = Translator.TranslateTextToMorse(message, new MorseCodeStyle('*', '=', ' ', '/'));

            // Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestCaseSource(typeof(TestCasesSourseTextMorseTranslators), nameof(TestCasesSourseTextMorseTranslators.TestCasesCustomMorseToText))]
        public void TranslateCustomMorseToText_ParametersAreValid_ReturnsCustomMorse(string message, string expectedResult)
        {
            // Act
            string actualResult = Translator.TranslateMorseToText(message, new MorseCodeStyle('*', '=', ' ', '/'));

            // Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestCaseSource(typeof(TestCasesSourceMorseBitArrayTranslators), nameof(TestCasesSourceMorseBitArrayTranslators.TestCasesStandartMorseToBitArray))]
        public void TranslateStandartMorseToBitArray_ParametersAreValid_ReturnsMorse(string message, BitArray expectedResult)
        {
            // Act
            BitArray actualResult = Translator.TranslateMorseToBitArray(message);

            // Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestCaseSource(typeof(TestCasesSourceMorseBitArrayTranslators), nameof(TestCasesSourceMorseBitArrayTranslators.TestCasesBitArrayToStandartMorse))]
        public void TranslateBitArrayToStandartMorse_ParametersAreValid_ReturnsBoolArray(BitArray message, string expectedResult)
        {
            // Act
            string actualResult = Translator.TranslateBitArrayToMorse(message);

            // Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestCaseSource(typeof(TestCasesSourceMorseBitArrayTranslators), nameof(TestCasesSourceMorseBitArrayTranslators.TestCasesCustonMorseToBitArray))]
        public void TranslateCustomMorseToBitArray_ParametersAreValid_ReturnsCustomMorse(string message, BitArray expectedResult)
        {
            // Act
            BitArray actualResult = Translator.TranslateMorseToBitArray(message, new MorseCodeStyle('*', '=', ' ', '/'));

            // Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestCaseSource(typeof(TestCasesSourceMorseBitArrayTranslators), nameof(TestCasesSourceMorseBitArrayTranslators.TestCasesBitArrayToCustomMorse))]
        public void TranslateBitArrayToCustomMorse_ParametersAreValid_ReturnsBoolArray(BitArray message, string expectedResult)
        {
            // Act
            string actualResult = Translator.TranslateBitArrayToMorse(message, new MorseCodeStyle('*', '=', ' ', '/'));

            // Assert
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
