﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

#pragma warning disable SA1118 // Parameter should not span multiple lines

namespace MorseCodeTranslator.Tests
{
    internal static class TestCasesSourseTextMorseTranslators
    {
        public static IEnumerable<TestCaseData> TestCasesTextToStandartMorse
        {
            get
            {
                yield return new TestCaseData(string.Empty, string.Empty);
                yield return new TestCaseData("A", ".-");
                yield return new TestCaseData("a", ".-");
                yield return new TestCaseData("hello", ".... . .-.. .-.. ---");
                yield return new TestCaseData("Hello", ".... . .-.. .-.. ---");
                yield return new TestCaseData("HELLO", ".... . .-.. .-.. ---");
                yield return new TestCaseData("Hello world", ".... . .-.. .-.. ---|.-- --- .-. .-.. -..");
                yield return new TestCaseData("Hello, world!", ".... . .-.. .-.. --- --..--|.-- --- .-. .-.. -.. -.-.--");
                yield return new TestCaseData(
                    "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'.,?!():+-=;/@_",
                    ".- -... -.-. -.. . ..-. --. .... .. .--- -.- .-.. -- -. --- .--. --.- .-. ... - ..- ...- .-- -..- -.-- --.. " +
                    ".---- ..--- ...-- ....- ..... -.... --... ---.. ----. ----- " +
                    ".----. .-.-.- --..-- ..--.. -.-.-- -.--. -.--.- ---... .-.-. -....- -...- -.-.-. -..-. .--.-. ..--.-");
                yield return new TestCaseData(
                    "Last Christmas, I gave you my heart, " +
                    "But the very next day, you gave it away. " +
                    "This year, to save me from tears " +
                    "I'll give it to someone special.",
                    ".-.. .- ... -|-.-. .... .-. .. ... - -- .- ... --..--|..|--. .- ...- .|-.-- --- ..-|-- -.--|.... . .- .-. - --..--|" +
                    "-... ..- -|- .... .|...- . .-. -.--|-. . -..- -|-.. .- -.-- --..--|-.-- --- ..-|--. .- ...- .|.. -|.- .-- .- -.-- .-.-.-|" +
                    "- .... .. ...|-.-- . .- .-. --..--|- ---|... .- ...- .|-- .|..-. .-. --- --|- . .- .-. ...|" +
                    ".. .----. .-.. .-..|--. .. ...- .|.. -|- ---|... --- -- . --- -. .|... .--. . -.-. .. .- .-.. .-.-.-");
            }
        }

        public static IEnumerable<TestCaseData> TestCasesStandartMorseToText
        {
            get
            {
                yield return new TestCaseData(string.Empty, string.Empty);
                yield return new TestCaseData(".-", "A");
                yield return new TestCaseData(".... . .-.. .-.. ---", "HELLO");
                yield return new TestCaseData(".... . .-.. .-.. ---|.-- --- .-. .-.. -..", "HELLO WORLD");
                yield return new TestCaseData(".... . .-.. .-.. --- --..--|.-- --- .-. .-.. -.. -.-.--", "HELLO, WORLD!");
                yield return new TestCaseData(
                    ".- -... -.-. -.. . ..-. --. .... .. .--- -.- .-.. -- -. --- .--. --.- .-. ... - ..- ...- .-- -..- -.-- --.. " +
                    ".---- ..--- ...-- ....- ..... -.... --... ---.. ----. ----- " +
                    ".----. .-.-.- --..-- ..--.. -.-.-- -.--. -.--.- ---... .-.-. -....- -...- -.-.-. -..-. .--.-. ..--.-",
                    "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'.,?!():+-=;/@_");
                yield return new TestCaseData(
                    ".-.. .- ... -|-.-. .... .-. .. ... - -- .- ... --..--|..|--. .- ...- .|-.-- --- ..-|-- -.--|.... . .- .-. - --..--|" +
                    "-... ..- -|- .... .|...- . .-. -.--|-. . -..- -|-.. .- -.-- --..--|-.-- --- ..-|--. .- ...- .|.. -|.- .-- .- -.-- .-.-.-|" +
                    "- .... .. ...|-.-- . .- .-. --..--|- ---|... .- ...- .|-- .|..-. .-. --- --|- . .- .-. ...|" +
                    ".. .----. .-.. .-..|--. .. ...- .|.. -|- ---|... --- -- . --- -. .|... .--. . -.-. .. .- .-.. .-.-.-",
                    "LAST CHRISTMAS, I GAVE YOU MY HEART, " +
                    "BUT THE VERY NEXT DAY, YOU GAVE IT AWAY. " +
                    "THIS YEAR, TO SAVE ME FROM TEARS " +
                    "I'LL GIVE IT TO SOMEONE SPECIAL.");
            }
        }

        public static IEnumerable<TestCaseData> TestCasesTextToCustomMorse
        {
            get
            {
                yield return new TestCaseData(string.Empty, string.Empty);
                yield return new TestCaseData("A", "*=");
                yield return new TestCaseData("a", "*=");
                yield return new TestCaseData("hello", "**** * *=** *=** ===");
                yield return new TestCaseData("Hello", "**** * *=** *=** ===");
                yield return new TestCaseData("HELLO", "**** * *=** *=** ===");
                yield return new TestCaseData("Hello world", "**** * *=** *=** ===/*== === *=* *=** =**");
                yield return new TestCaseData("Hello, world!", "**** * *=** *=** === ==**==/*== === *=* *=** =** =*=*==");
                yield return new TestCaseData(
                    "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'.,?!():+-=;/@_",
                    "*= =*** =*=* =** * **=* ==* **** ** *=== =*= *=** == =* === *==* ==*= *=* *** = **= ***= *== =**= =*== ==** " +
                "*==== **=== ***== ****= ***** =**** ==*** ===** ====* ===== " +
                "*====* *=*=*= ==**== **==** =*=*== =*==* =*==*= ===*** *=*=* =****= =***= =*=*=* =**=* *==*=* **==*=");
                yield return new TestCaseData(
                    "Last Christmas, I gave you my heart, " +
                    "But the very next day, you gave it away. " +
                    "This year, to save me from tears " +
                    "I'll give it to someone special.",
                    "*=** *= *** =/=*=* **** *=* ** *** = == *= *** ==**==/**/==* *= ***= */=*== === **=/== =*==/**** * *= *=* = ==**==/" +
                "=*** **= =/= **** */***= * *=* =*==/=* * =**= =/=** *= =*== ==**==/=*== === **=/==* *= ***= */** =/*= *== *= =*== *=*=*=/" +
                "= **** ** ***/=*== * *= *=* ==**==/= ===/*** *= ***= */== */**=* *=* === ==/= * *= *=* ***/" +
                "** *====* *=** *=**/==* ** ***= */** =/= ===/*** === == * === =* */*** *==* * =*=* ** *= *=** *=*=*=");
            }
        }

        public static IEnumerable<TestCaseData> TestCasesCustomMorseToText
        {
            get
            {
                yield return new TestCaseData(string.Empty, string.Empty);
                yield return new TestCaseData("*=", "A");
                yield return new TestCaseData("**** * *=** *=** ===", "HELLO");
                yield return new TestCaseData("**** * *=** *=** ===/*== === *=* *=** =**", "HELLO WORLD");
                yield return new TestCaseData("**** * *=** *=** === ==**==/*== === *=* *=** =** =*=*==", "HELLO, WORLD!");
                yield return new TestCaseData(
                    "*= =*** =*=* =** * **=* ==* **** ** *=== =*= *=** == =* === *==* ==*= *=* *** = **= ***= *== =**= =*== ==** " +
                    "*==== **=== ***== ****= ***** =**** ==*** ===** ====* ===== " +
                    "*====* *=*=*= ==**== **==** =*=*== =*==* =*==*= ===*** *=*=* =****= =***= =*=*=* =**=* *==*=* **==*=",
                    "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'.,?!():+-=;/@_");
                yield return new TestCaseData(
                    "*=** *= *** =/=*=* **** *=* ** *** = == *= *** ==**==/**/==* *= ***= */=*== === **=/== =*==/**** * *= *=* = ==**==/" +
                    "=*** **= =/= **** */***= * *=* =*==/=* * =**= =/=** *= =*== ==**==/=*== === **=/==* *= ***= */** =/*= *== *= =*== *=*=*=/" +
                    "= **** ** ***/=*== * *= *=* ==**==/= ===/*** *= ***= */== */**=* *=* === ==/= * *= *=* ***/" +
                    "** *====* *=** *=**/==* ** ***= */** =/= ===/*** === == * === =* */*** *==* * =*=* ** *= *=** *=*=*=",
                    "LAST CHRISTMAS, I GAVE YOU MY HEART, " +
                    "BUT THE VERY NEXT DAY, YOU GAVE IT AWAY. " +
                    "THIS YEAR, TO SAVE ME FROM TEARS " +
                    "I'LL GIVE IT TO SOMEONE SPECIAL.");
            }
        }
    }
}
